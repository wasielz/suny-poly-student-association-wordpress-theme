<header class="banner" role="banner">

  <div class="container">

    <nav id="primary-navigation" class="navbar navbar-sunyit navbar-static-top yamm" role="navigation">
			
			<div class="navbar-header">
				<button class="navbar-btn btn btn-primary btn-outline btn-lg" data-toggle="collapse" data-target=".navbar-collapse">
					Menu
				</button>
				<a class="navbar-brand" href="/"><?php bloginfo('name'); ?></a>		
			</div>
    
      <div class="collapse navbar-collapse">

				<div class="navbar-sidebar">
					<?php dynamic_sidebar('sidebar-header'); ?>
				</div>

        <?php
          if (has_nav_menu('primary_navigation')) :
            //wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
          endif;
        ?>

				<ul class="nav navbar-nav">
					<li class="dropdown yamm-fw menu-future-students">
						<a class="dropdown-toggle" data-hover="dropdown" data-delay="300" href="/prospective">Future Students</a>
						<ul class="dropdown-menu hidden-sm hidden-xs">
							<li>
								<div class="yamm-content">
									<h2>Future Students</h2>
									<div class="row">
										<div class="col-lg-2 col-md-2">
											<h5><a href="/prospective/">future.sunyit.edu</a></h5>
											<ul class="list-unstyled highlight">
												<li><a href="/admissions/">Admissions</a></li>
												<li><a href="/apply/">Apply Now</a></li>
												<li><a href="mailto:admissions@sunyit.edu">Contact Us</a></li>
												<li><a href="/directions/">Directions to SUNYIT</a></li>
												<li><a href="/apps/calendar/main.php?calendar=admissions" target="_blank">Meet Us in Your Area</a></li>
												<li><a href="/apps/form/?resource=admissions_request_moreinfo">Request Information</a></li>
												<li><a href="/virtualtour/">Take a Virtual Tour</a></li>
												<li><a href="/apps/form/?resource=admissions_schedule_visit" target="_blank">Schedule a Visit</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Admissions</h5>
											<ul class="list-unstyled">
												<li><a href="/admissions">Undergraduate</a></li>
												<li><a href="/graduate_center/">Graduate</a></li>
												<li><a href="/international_admissions/">International</a></li>
											</ul>
											<h5>General Information</h5>
											<ul class="list-unstyled">
												<li><a href="/about/">About SUNYIT</a></li>
												<li><a href="/bursar/billing">Billing/Tuition</a></li>
												<li><a href="https://ssl.sunyit.edu/apps/weblog/">Blogs</a></li>
												<li><a href="/bookstore">Bookstore</a></li>
												<li><a href="/tuition/">Cost of Attendance</a></li>
												<li><a href="/directions/">Directions/Maps</a></li>
												<li><a href="/mailroom/addresses">Mailing Address</a></li>
												<li><a href="/regional_information/">Regional Information</a></li>
												<li><a href="/university_police/safety">Safety Information/<br>
												Crime Stats</a></li>
												<li><a href="/tuition/">Tuition</a></li>
												<li><a href="/videos/">Videos</a></li>
												<li><a href="/virtualtour/">Virtual Tour</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Academics</h5>
											<ul class="list-unstyled">
												<li><a href="/calendars_events/academic_calendars">Academic Calendars</a></li>
												<li><a href="/accreditation/">Accreditation</a></li>
												<li><a href="/apply/">Applications</a></li>
												<li><a href="/articulation/">Articulation Agreements</a></li>
												<li><a href="/catalogs/">Catalogs</a></li>
												<li><a href="/calendars_events/course_exam">Course Schedules</a></li>
												<li><a href="/online/">Online Learning</a></li>
											</ul>
											<h5>Majors/Programs</h5>
											<ul class="list-unstyled">
												<li><a href="/programs/undergraduate">Undergraduate</a></li>
												<li><a href="/graduate_center/programs">Graduate</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Student Life</h5>
											<ul class="list-unstyled">
												<li><a href="/wildcats/">Athletics</a></li>
												<li><a href="/clubs/">Clubs &amp; Organizations</a></li>
												<li><a href="/apps/calendar/main.php?view=week">Events Calendar</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/intramurals/">Intramurals</a></li>
												<li><a href="/campus_living/">Living on Campus</a></li>
												<li><a href="/residential_life/">Residential Life</a></li>
												<li><a href="/student_activities/">Student Activities</a></li>
											</ul>
											<h5>Special Events</h5>
											<ul class="list-unstyled">
												<li><a href="/open_house/">Open House</a></li>
												<li><a href="/orientation/">Orientation</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Services</h5>
											<ul class="list-unstyled">
												<li><a href="/admissions/">Admissions</a></li>
												<li><a href="/bookstore" target="_blank">Bookstore</a></li>
												<li><a href="/bursar/">Bursar</a></li>
												<li><a href="http://www.careerservices.sunyit.edu/">Career Services</a></li>
												<li><a href="/counseling/">Counseling Center</a></li>
												<li><a href="/disability_services/">Disability Services</a></li>
												<li><a href="/eop/">EOP</a></li>
												<li><a href="/financial_aid/">Financial Aid</a></li>
												<li><a href="http://www.sunyitdiningservices.com/" target="_blank">Food Services<br>
												(Sodexo)</a></li>
												<li><a href="/health_wellness/">Health &amp; Wellness</a></li>
												<li><a href="/learning_center/">Learning Center</a></li>
												<li><a href="/registrar/">Registrar</a></li>
												<li><a href="/veteran_benefits/">Veteran Benefits</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>For Parents</h5>
											<ul class="list-unstyled">
												<li><a href="/accommodations/">Accommodations</a></li>
												<li><a href="https://ssl.sunyit.edu/apps/weblog/?blog=news&amp;mode=viewposts">News</a></li>
												<li><a href="https://banner.sunyit.edu/pls/prod/twbkwbis.P_WWWLogin">NY Alert</a></li>
											</ul>
											<h5>K-12 Programs</h5>
											<ul class="list-unstyled">
												<li><a href="/cpe_k_12/fll">First Lego League</a></li>
												<li><a href="/mvtech-showcase">Tech Showcase</a></li>
												<li><a href="/summeriteens">SummerITeens</a></li>
											</ul>
											<h5>Adult Learners</h5>
											<ul class="list-unstyled">
												<li><a href="/cpe">Continuing Professional Education</a></li>
												<li><a href="/mvilr/">MVILR</a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown yamm-fw menu-current-students">
						<a class="dropdown-toggle" data-hover="dropdown" data-delay="300" href="/current">Current Students</a>
						<ul class="dropdown-menu hidden-sm hidden-xs">
							<li>
								<div class="yamm-content">
									<h2>Current Students</h2>
									<div class="row">
										<div class="col-lg-2 col-md-2">
											<h5><a href="/current/">current.sunyit.edu</a></h5>
											<ul class="list-unstyled highlight">
												<li><a href="http://sunyit.sln.suny.edu/" target="_blank">ANGEL</a></li>
												<li><a href="http://banner.sunyit.edu/">Banner</a></li>
												<li><a href="/bursar/billing">Billing Information</a></li>
												<li><a href="/bookstore" target="_blank">Bookstore</a></li>
												<li><a href="/apps/canceled_classes/">Canceled Classes</a></li>
												<li><a href="/calendars_events/course_exam">Course Schedules</a></li>
												<li><a href="http://webmail.sunyit.edu/">SUNYIT Email</a><a href="http://gmail.sunyit.edu/"><br></a></li>
												<li><a href="/financial_aid/">Financial Aid</a></li>
												<li><a href="/its/helpdesk/">Help Desk</a></li>
												<li><a href="http://helpdesk.sunyit.edu/">Help Desk Ticket</a></li>
												<li><a href="/online/">Online Learning</a></li>
												<li><a href="/payment/">Pay Your Bill</a></li>
												<li><a href="/bursar/quikpay_information">QuikPay Information</a></li>
												<li><a href="http://banner.sunyit.edu/pls/prod/twbkwbis.P_GenMenu?name=homepage">Register for Classes</a></li>
												<li><a href="/registrar/transcript">Transcript Request</a></li>
												<li><a href="/tuition/">Tuition</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Academics</h5>
											<ul class="list-unstyled">
												<li><a href="/calendars_events/academic_calendars">Academic Calendars</a></li>
												<li><a href="/academic_support.departments">Academic Departments</a></li>
												<li><a href="/academic_support">Academic Support</a></li>
												<li><a href="/accreditation/">Accreditation</a></li>
												<li><a href="/articulation/">Articulation</a></li>
												<li><a href="/catalogs/">Catalogs</a></li>
												<li><a href="/calendars_events/course_exam">Course Schedules</a></li>
												<li><a href="/library">Library</a></li>
												<li><a href="/online/">Online Learning</a></li>
												<li><a href="/registrar/transcript">Transcript Request</a></li>
											</ul>
											<h5>Majors/Programs</h5>
											<ul class="list-unstyled">
												<li><a href="/programs/undergraduate">Undergraduate</a></li>
												<li><a href="/graduate_center/programs">Graduate</a></li>
											</ul>
											<h5>Administration</h5>
											<ul class="list-unstyled">
												<li><a href="/president/">President</a></li>
												<li><a href="/provost/">Provost</a></li>
												<li><a href="/vp_administration/">VP Administration</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Student Services</h5>
											<ul class="list-unstyled">
												<li><a href="/admissions/">Admissions</a></li>
												<li><a href="/bookstore/">Bookstore</a></li>
												<li><a href="/bursar/">Bursar</a></li>
												<li><a href="/careerservices">Career Services</a></li>
												<li><a href="/counseling/">Counseling Center</a></li>
												<li><a href="/disability_services">Disability Services</a></li>
												<li><a href="/eop/">EOP</a></li>
												<li><a href="/financial_aid/">Financial Aid</a></li>
												<li><a href="http://www.sunyitdiningservices.com/" target="_blank">Food Services<br>
												(Sodexo)<br></a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Student Services</h5>
											<ul class="list-unstyled">
												<li><a href="/graduate_center/">Graduate Center</a></li>
												<li><a href="/health_wellness/">Health &amp; Wellness</a></li>
												<li><a href="/instructional_resources/">Instructional Resources</a></li>
												<li><a href="/instructional_technology/">Instructional Technology</a></li>
												<li><a href="/iss/">International Students</a></li>
												<li><a href="/its">IT Services</a></li>
												<li><a href="/learning_center/">Learning Center</a></li>
												<li><a href="/library/">Library</a></li>
												<li><a href="/registrar/">Registrar</a></li>
												<li><a href="/residential_life/">Residential Life</a></li>
												<li><a href="/university_police/">University Police</a></li>
												<li><a href="/veteran_benefits/">Veteran Benefits</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Student Life</h5>
											<ul class="list-unstyled">
												<li><a href="/wildcats/">Athletics</a></li>
												<li><a href="/clubs/">Clubs &amp; Organizations</a></li>
												<li><a href="/campus_living/">Campus Living</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/intramurals/">Intramurals</a></li>
												<li><a href="/student_association/">Student Association</a></li>
												<li><a href="/student_activities/">Student Activities</a></li>
												<li><a href="/campus_living/voter_registration">Voter Registration</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>General Information</h5>
											<ul class="list-unstyled">
												<li><a href="http://blogs.sunyit.edu/">Blogs</a></li>
												<li><a href="/apps/canceled_classes/">Canceled Classes</a></li>
												<li><a href="/commencement/">Commencement</a></li>
												<li><a href="/directions/">Directions/Maps</a></li>
												<li><a href="http://ssl.sunyit.edu/apps/directory/">Directories</a></li>
												<li><a href="/giving/">Give to SUNYIT</a></li>
												<li><a href="/mailroom/addresses">Mailing Address</a></li>
												<li><a href="http://ssl.sunyit.edu/apps/weblog/?blog=news&amp;mode=viewposts">News</a></li>
												<li><a href="http://banner.sunyit.edu/pls/prod/twbkwbis.P_WWWLogin">NY Alert</a></li>
												<li><a href="/giving/phonathon">Phonathon</a></li>
												<li><a href="/photo_gallery/">Photo Gallery</a></li>
												<li><a href="/university_police/safety">Safety Information/<br>
												Crime Stats</a></li>
												<li><a href="/pdf/student_handbook.pdf">Student Handbook/<br>
												Code of Conduct</a> (pdf)<a href="/pdf/student_handbook.pdf"><br></a></li>
												<li><a href="/videos/">Videos</a></li>
												<li><a href="/titleix/">Title IX</a></li>
												<li><a href="/virtualtour/">Virtual Tour</a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown yamm-fw menu-alumni-friends">
						<a class="dropdown-toggle" data-hover="dropdown" data-delay="300" href="/alumni">Alumni &amp; Friends</a>
						<ul class="dropdown-menu hidden-sm hidden-xs">
							<li>
								<div class="yamm-content">
									<h2>Alumni &amp; Friends</h2>
									<div class="row">
										<div class="col-lg-2 col-md-2">
											<h5><a href="/alumni/">Alumni home</a></h5>
											<ul class="list-unstyled highlight">
												<li><a href="http://www.sunyit.edu/apps/directory/?group=alumni&amp;format=short">Contact Us</a></li>
												<li><a href="/accommodations/">Accommodations</a></li>
												<li><a href="/directions/">Directions/Maps</a></li>
												<li><a href="/giving/">Give to SUNYIT</a></li>
												<li><a href="/regional_information/">Regional Information</a></li>
												<li><a href="/payment/">Pay Online</a></li>
												<li><a href="http://gmail.sunyit.edu/">SUNYIT Email</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Alumni</h5>
											<ul class="list-unstyled">
												<li><a href="http://www.sunyit.edu/alumni.find_alumni">Find Alumni</a></li>
												<li><a href="/alumni.submitnews" target="_blank">Submit Alumni News</a></li>
												<li><a href="http://www.sunyit.edu/alumni/transcript_request">Transcript Request</a></li>
												<li><a href="/alumni.the_bridge">View “The Bridge”</a></li>
												<li><a href="/alumni/visit_us">Visit Us</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Alumni</h5>
											<ul class="list-unstyled">
												<li><a href="/alumni/awards">Awards</a></li>
												<li><a href="/alumni/member_benefits">Benefits</a></li>
												<li><a href="/alumni/board_of_directors">Board</a></li>
												<li><a href="http://careerservices.sunyit.edu/">Career Services</a></li>
												<li><a href="/alumni/event_photos">Event Photos</a></li>
												<li><a href="/alumni/groups">Groups</a></li>
												<li><a href="/alumni/upcoming_events">Upcoming Events</a></li>
												<li><a href="/alumni/volunteering">Volunteer</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Friends &amp; Visitors</h5>
											<ul class="list-unstyled">
												<li><strong>General Information</strong></li>
												<li><a href="/about/">About SUNYIT</a></li>
												<li><a href="/wildcats/">Athletics</a></li>
												<li><a href="/bookstore" target="_blank">Bookstore</a></li>
												<li><a href="/president/college_council_members">College Council</a></li>
												<li><a href="/commencement/">Commencement</a></li>
												<li><a href="http://web.sunyit.edu/apps/directory/">Directories</a></li>
												<li><a href="/employment/">Employment</a></li>
												<li><a href="/gannett_gallery/">Gannett Gallery</a></li>
												<li><a href="/mailroom/addresses">Mailing Address</a></li>
												<li><a href="http://ssl.sunyit.edu/apps/weblog/?blog=news&amp;mode=viewposts">News</a></li>
												<li><a href="/photo_gallery/">Photo Gallery</a></li>
												<li><a href="/regional_information/">Regional Information</a></li>
												<li><a href="/virtualtour/">Virtual Tour</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Friends &amp; Visitors</h5>
											<ul class="list-unstyled">
												<li><strong>Offices</strong></li>
												<li><a href="/admissions/">Admissions</a></li>
												<li><a href="http://www.cpe.sunyit.edu/">Continuing Professional Education</a></li>
												<li><a href="/corporate_events/">Corporate Events</a></li>
												<li><a href="/foundation/">Foundation</a></li>
												<li><a href="/graduate_center/">Graduate Center</a></li>
												<li><a href="/health_wellness/">Health &amp; Wellness Center</a></li>
												<li><a href="/mvilr/">MVILR</a></li>
												<li><a href="/president/">President</a></li>
												<li><a href="/public_relations/">Public Affairs</a></li>
												<li><a href="/publications/">Publications</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Friends &amp; Visitors</h5>
											<ul class="list-unstyled">
												<li><strong>Offices</strong></li>
												<li><a href="/registrar/">Registrar</a></li>
												<li><a href="/sbdc/">SBDC</a></li>
												<li><a href="/sponsored_research/">Sponsored Research</a></li>
												<li><a href="/veteran_benefits/">Veteran Benefits</a></li>
											</ul>
											<h5>K-12 Programs</h5>
											<ul class="list-unstyled">
												<li><a href="/cpe_k_12/fll">First Lego League</a></li>
												<li><a href="/mvtech-showcase">Tech Showcase</a></li>
												<li><a href="/summeriteens">SummerITeens</a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown yamm-fw menu-faculty-staff">
						<a class="dropdown-toggle" data-hover="dropdown" data-delay="300" href="/faculty_staff">Faculty &amp; Staff</a>
						<ul class="dropdown-menu hidden-sm hidden-xs">
							<li>
								<div class="yamm-content">
									<h2>Faculty &amp; Staff</h2>
									<div class="row">
										<div class="col-lg-2 col-md-2">
											<h5><a href="/faculty_staff/">Faculty &amp; Staff home</a></h5>
											<ul class="list-unstyled highlight">
												<li><a href="http://sunyit.sln.suny.edu/" target="_blank">ANGEL</a></li>
												<li><a href="http://banner.sunyit.edu/">Banner</a></li>
												<li><a href="http://ssl.sunyit.edu/apps/weblog/">Blogs</a></li>
												<li><a href="/apps/canceled_classes">Canceled Classes</a></li>
												<li><a href="/calendars_events/course_exam">Course Schedules</a></li>
												<li><a href="/its/helpdesk/">Help Desk</a></li>
												<li><a href="http://helpdesk.sunyit.edu/">Help Desk Ticket</a></li>
												<li><a href="/library/databases">Library Databases</a></li>
												<li><a href="/online/">Online Learning</a></li>
												<li><a href="http://webmail.sunyit.edu/">SUNYIT Email</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>General Information</h5>
											<ul class="list-unstyled">
												<li><a href="/wildcats/">Athletics</a></li>
												<li><a href="/university_police/safety">Crime Stats</a></li>
												<li><a href="/directions/">Directions/Maps</a></li>
												<li><a href="/employment/">Employment</a></li>
												<li><a href="/faculty_assembly">Faculty Assembly</a></li>
												<li><a href="/pdf/faculty/faculty_handbook.pdf">Faculty Handbook (pdf)</a></li>
												<li><a href="/facultyresources">Faculty Resources</a></li>
												<li><a href="http://www.internal.sunyit.edu/">Internal</a></li>
												<li><a href="http://ssl.sunyit.edu/apps/weblog/?blog=news&amp;mode=viewposts">News</a></li>
												<li><a href="http://banner.sunyit.edu/pls/prod/twbkwbis.P_WWWLogin">NY Alert</a></li>
												<li><a title="Phone List - Fall 2013 - Faculty, Staff, Offices" href="/home5/cmsfiles/sunyit2010.mainmenu/SUNYITphonelist-fall2013.pdf">Phone List</a> (pdf)</li>
												<li><a href="/photo_gallery/">Photo Gallery</a></li>
												<li><a href="/policies">Policies</a></li>
												<li><a href="/staff_assembly">Staff Assembly</a></li>
												<li><a href="/pdf/student_handbook.pdf">Student Handbook/<br>
												Code of Conduct</a> (pdf)<a href="/pdf/student_handbook.pdf"><br></a></li>
												<li><a href="/usage">Style/Usage Guide</a></li>
												<li><a href="/titleix/">Title IX</a></li>
												<li><a href="/bursar/billing">Tuition/Billing</a></li>
											</ul>
											<h5>Announcements</h5>
											<ul class="list-unstyled">
												<li><a href="http://www.sunyit.edu/apps/weblog/?blog=faculty&amp;mode=viewposts">Faculty</a></li>
												<li><a href="http://www.sunyit.edu/apps/weblog/?blog=staff&amp;mode=viewposts">Staff</a></li>
												<li><a href="http://www.sunyit.edu/apps/weblog/?blog=student&amp;mode=viewposts">Students</a></li>
											</ul>
											<p>&nbsp;</p>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Academics</h5>
											<ul class="list-unstyled">
												<li><a href="/calendars_events/academic_calendars">Academic Calendars</a></li>
												<li><a href="/academic_support.departments">Academic Departments</a></li>
												<li><a href="/academic_support">Academic Support</a></li>
												<li><a href="/accreditation/">Accreditation</a></li>
												<li><a href="/articulation/">Articulation</a></li>
												<li><a href="/catalogs/">Catalogs</a></li>
												<li><a href="/calendars_events/course_exam">Course Schedules</a></li>
												<li><a href="/online/">Online Learning</a></li>
												<li><a href="/programs/">Programs/Majors</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Offices</h5>
											<ul class="list-unstyled">
												<li><a href="/admissions/">Admissions</a></li>
												<li><a href="/bookstore" target="_blank">Bookstore</a></li>
												<li><a href="/bursar/">Bursar</a></li>
												<li><a href="/business_affairs/">Business Affairs</a></li>
												<li><a href="http://careerservices.sunyit.edu/">Career Services</a></li>
												<li><a href="http://www.cpe.sunyit.edu/" target="_blank">Continuing Professional Education</a></li>
												<li><a href="/counseling/">Counseling Center</a></li>
												<li><a href="/corporate_events">Corporate Events</a></li>
												<li><a href="/disability_services">Disability Services</a></li>
												<li><a href="/environmental_health_and_safety/">Environmental Health &amp; Safety</a></li>
												<li><a href="/eop/">EOP</a></li>
												<li><a href="/facilities/">Facilities</a></li>
												<li><a href="/financial_aid/">Financial Aid</a></li>
												<li><a href="/food_services/" target="_blank">Food Services<br>
												(Sodexo)</a></li>
												<li><a href="/foundation/">Foundation</a></li>
												<li><a href="/graduate_center/">Graduate Center</a></li>
												<li><a href="/health_wellness/">Health &amp; Wellness Center</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Offices</h5>
											<ul class="list-unstyled">
												<li><a href="/human_resources/">Human Resources</a></li>
												<li><a href="/institutional_research/">Institutional Research</a></li>
												<li><a href="/instructional_resources/">Instructional Resources</a></li>
												<li><a href="/instructional_technology/">Instructional Technology</a></li>
												<li><a href="/iss/">International Students</a></li>
												<li><a href="/its/">IT Services</a></li>
												<li><a href="/learning_center/">Learning Center</a></li>
												<li><a href="/mailroom/">Mailroom</a></li>
												<li><a href="/president/">President</a></li>
												<li><a href="/provost/">Provost</a></li>
												<li><a href="/public_relations/">Public Affairs</a></li>
												<li><a href="/publications_office/">Publications</a></li>
												<li><a href="/registrar/">Registrar</a></li>
												<li><a href="/residential_life/">Residential Life</a></li>
												<li><a href="/sponsored_research/">Sponsored Research</a></li>
												<li><a href="/student_activities/">Student Activities</a></li>
												<li><a href="/student_affairs/">Student Affairs</a></li>
												<li><a href="/university_police/">University Police</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Administration</h5>
											<ul class="list-unstyled">
												<li><a href="/president/">President</a></li>
												<li><a href="/provost/">Provost</a></li>
												<li><a href="/vp_administration/">VP Administration</a></li>
											</ul>
											<h5>Special Events</h5>
											<ul class="list-unstyled">
												<li><a href="/commencement/">Commencement</a></li>
												<li><a href="/open_house/">Open House</a></li>
												<li><a href="/orientation/">Orientation</a></li>
											</ul>
											<h5>IT Services</h5>
											<ul class="list-unstyled">
												<li><a href="http://banner.sunyit.edu/">Banner</a></li>
												<li><a href="/its/helpdesk/user_documentation">Help &amp; Tutorials</a></li>
												<li><a href="/its/helpdesk/">Help Desk</a></li>
												<li><a href="http://helpdesk.sunyit.edu/">Help Desk Ticket</a></li>
												<li><a href="/its/">IT Services</a></li>
												<li><a href="http://gmail.sunyit.edu/">SUNYIT Gmail</a></li>
												<li><a href="http://list.sunyit.edu/">Sympa Listserv</a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown yamm-fw menu-online-learners">
						<a class="dropdown-toggle" data-hover="dropdown" data-delay="300" href="/online">Online Learners</a>
						<ul class="dropdown-menu hidden-sm hidden-xs">
							<li>
								<div class="yamm-content">
									<h2>Online Learners</h2>
									<div class="row">
										<div class="col-lg-2 col-md-2">
											<h5><a href="/online/">Online Learning home</a></h5>
											<ul class="list-unstyled highlight">
												<li><a href="http://sunyit.sln.suny.edu/" target="_blank">ANGEL</a></li>
												<li><a href="mailto:online@sunyit.edu?subject=Online%20Learning%20question">Contact Us</a></li>
												<li><a href="/online/get_started">Get Started</a></li>
												<li><a href="/online/help">Login Help</a></li>
												<li><a href="/registrar/instructions">Register for Classes</a></li>
												<li><a href="http://sunyit.sln.suny.edu/signon/RequestPasswordReset.aspx" target="_blank">Reset Guest ANGEL Password</a></li>
												<li><a href="http://banner.sunyit.edu/pls/prod/twbkwbis.P_ValLogin">Reset SITNet Password</a></li>
												<li><a href="/its/helpdesk/user_documentation/sitnet_username">SITNet Username Lookup</a></li>
												<li><a href="http://webmail.sunyit.edu/">SUNYIT Email</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Student Resources</h5>
											<ul class="list-unstyled">
												<li><a href="/academic_support.departments">Academic Departments</a></li>
												<li><a href="/academic_support">Academic Support</a></li>
												<li><a href="/online/help">Help for Students</a></li>
												<li><a href="/instructional_technology">Instructional Technology</a></li>
												<li><a href="http://support8.sln.suny.edu/default.asp" target="_blank">SLN Student Orientation</a></li>
												<li><a href="http://sunyit.sln.suny.edu/Help/Public/frameset_resources.htm">SLN Helpdesk Policies</a></li>
											</ul>
											<h5>Technology &amp;<br>
											Other Needs</h5>
											<ul class="list-unstyled">
												<li><a href="/online/software_downloads">Software Downloads</a></li>
												<li><a href="/online/technical_requirements">Technical Requirements</a><a href="/online/help"><br></a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Faculty Resources</h5>
											<ul class="list-unstyled">
												<li><a href="/academic_support.departments">Academic Departments</a></li>
												<li><a href="/academic_support">Academic Support</a></li>
												<li><a href="http://www.sunyit.edu/home5/cmsfiles/online.help.faculty/Faculty_Guide_to_Accessing_ANGEL_Courses.pdf" target="_blank">Accessing ANGEL Courses - Faculty Guide (pdf)</a></li>
												<li><a href="/its/policies/faculty_online_learning_procedures/">ANGEL Procedures</a></li>
												<li><a href="/library/ereserves">E-Reserves Procedures</a></li>
												<li><a href="/online/help/faculty">Help for Instructors</a></li>
												<li><a href="/instructional_technology">Instructional Technology</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Prospective Students</h5>
											<ul class="list-unstyled">
												<li><a href="http://sln.suny.edu/sln_courselistings.htm" target="_blank">Courses &amp; Descriptions</a></li>
												<li><a href="/online/degrees">Degree Programs</a></li>
												<li><a href="http://support8.sln.suny.edu/default.asp" target="_blank">Demo a Course</a></li>
												<li><a href="http://sln.suny.edu/help/help_helpdeskfaq.shtml" target="_blank">Online Learning FAQs</a></li>
												<li><a href="http://sln.suny.edu/gs/gs_rightforyou.shtml" target="_blank">Is online learning right for you?</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>General Information</h5>
											<ul class="list-unstyled">
												<li><a href="/calendars_events/academic_calendars">Academic Calendar</a></li>
												<li><a href="/accreditation/">Accreditation</a></li>
												<li><a href="/admissions/">Admissions</a></li>
												<li><a href="/bookstore" target="_blank">Bookstore</a></li>
												<li><a href="http://careerservices.sunyit.edu/">Career Services</a></li>
												<li><a href="/calendars_events/course_exam">Course Schedules</a></li>
												<li><a href="/directions/">Directions/Maps</a></li>
												<li><a href="/disability_services/">Disability Services</a></li>
												<li><a href="/financial_aid/">Financial Aid</a></li>
												<li><a href="/graduate_center/">Graduate Center</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>General Information</h5>
											<ul class="list-unstyled">
												<li><a href="/its/helpdesk/">Help Desk</a></li>
												<li><a href="http://its.sunyit.edu/">IT Services</a></li>
												<li><a href="http://banner.sunyit.edu/pls/prod/twbkwbis.P_WWWLogin">NY Alert</a></li>
												<li><a href="/payment/">Pay Bills Online</a></li>
												<li><a href="/registrar/instructions">Registration Instructions</a></li>
												<li><a href="/titleix/">Title IX</a></li>
												<li><a href="/tuition/">Tuition</a></li>
												<li><a href="/virtualtour/">Virtual Tour</a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown yamm-fw menu-majors-academics">
						<a class="dropdown-toggle" data-hover="dropdown" data-delay="300" href="/programs">Majors &amp; Academics</a>
						<ul class="dropdown-menu hidden-sm hidden-xs">
							<li>
								<div class="yamm-content">
									<h2>Majors &amp; Academics</h2>
									<div class="row">
										<div class="col-lg-2 col-md-2">
											<h5><a href="/programs/">majors.sunyit.edu</a></h5>
											<ul class="list-unstyled highlight">
												<li><a href="/calendars_events/academic_calendars">Academic Calendars</a></li>
												<li><a href="/academic_support.departments">Academic Departments</a></li>
												<li><a href="/academic_support">Academic Support</a></li>
												<li><a href="/accreditation/">Accreditation</a></li>
												<li><a href="http://sunyit.sln.suny.edu/" target="_blank">ANGEL</a></li>
												<li><a href="/articulation/">Articulation Agreements</a></li>
												<li><a href="http://banner.sunyit.edu">Banner</a></li>
												<li><a href="/apps/canceled_classes/">Canceled Classes</a></li>
												<li><a href="/catalogs/">Catalogs</a></li>
												<li><a href="/calendars_events/course_exam">Course Schedules</a></li>
												<li><a href="/library/">Library</a></li>
												<li><a href="/online/">Online Learning</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Majors/Programs</h5>
											<ul class="list-unstyled">
												<li><strong>Undergraduate</strong></li>
												<li><a href="/undergraduate/acc/">Accounting</a></li>
												<li><a href="/undergraduate/applied_computing">Applied Computing</a></li>
												<li><a href="/undergraduate/mat/">Applied Mathematics</a></li>
												<li><a href="/undergraduate/bio">Biology</a></li>
												<li><a href="/undergraduate/bus/">Business Administration</a></li>
												<li><a href="/undergraduate/civ/">Civil Engineering</a></li>
												<li><a href="/undergraduate/ctc/">Civil Engineering Technology</a></li>
												<li><a href="/undergraduate/cid/">Communication &amp; Information Design</a></li>
												<li><a href="/undergraduate/cbh/">Community &amp; Behavioral Health</a></li>
												<li><a href="/undergraduate/cet/">Computer Engineering Technology</a></li>
												<li><a href="/undergraduate/csc/">Computer &amp; Information Science</a></li>
												<li><a href="/undergraduate/cis/">Computer Information Systems</a></li>
												<li><a href="/undergraduate/cj/">Criminal Justice</a></li>
												<li><a href="/undergraduate/ece/">Electrical &amp; Computer Engineering</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Majors/Programs</h5>
											<ul class="list-unstyled">
												<li><strong>Undergraduate</strong></li>
												<li><a href="/undergraduate/etc/">Electrical Engineering Technology</a></li>
												<li><a href="/undergraduate/fin/">Finance</a></li>
												<li><a href="/undergraduate/gen/">General Studies</a></li>
												<li><a href="/undergraduate/him/">Health Information Management</a></li>
												<li><a href="/undergraduate/hsm/">Health Services Management</a></li>
												<li><a href="/undergraduate/iet/">Industrial Engineering Technology</a></li>
												<li><a href="/undergraduate/ids/">Interdisciplinary Studies</a></li>
												<li><a href="/undergraduate/me/">Mechanical Engineering</a></li>
												<li><a href="/undergraduate/mtc/">Mechanical Engineering Technology</a></li>
												<li><a href="/undergraduate/ncs">Network &amp; Computer Security</a></li>
												<li><a href="/undergraduate/nur/">Nursing</a></li>
												<li><a href="/undergraduate/psy/">Psychology</a></li>
												<li><a href="/undergraduate/soc/">Sociology</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Graduate</h5>
											<ul class="list-unstyled">
												<li><strong>MS Programs</strong></li>
												<li><a href="/programs/graduate/msacc/">Accountancy</a></li>
												<li><a href="/programs/graduate/atc/">Advanced Technology</a></li>
												<li><a href="/programs/graduate/nur/adult_nurse_prac">Adult Nurse Practitioner</a></li>
												<li><a href="/programs/graduate/soc/">Applied Sociology</a></li>
												<li><a href="/programs/graduate/cis/">Computer &amp; Information Science</a></li>
												<li><a href="/programs/graduate/nur/family_nurse_prac">Family Nurse Practitioner</a></li>
												<li><a href="/programs/graduate/nur/gerontology_nurse_prac">Gerontological Nurse Practitioner</a></li>
												<li><a href="/programs/graduate/hsa/">Health Services Administration</a></li>
												<li><a href="/programs/graduate/idt/">Information Design &amp; Technology</a></li>
												<li><a href="/programs/graduate/ncs/">Network and Computer Security</a></li>
												<li><a href="/programs/graduate/nur/nurse_admin">Nursing Administration</a></li>
												<li><a href="/programs/graduate/nursinged">Nursing Education</a></li>
												<li><a href="/programs/graduate/tel/">Telecommunications</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Graduate</h5>
											<ul class="list-unstyled">
												<li><strong>MBA Programs</strong></li>
												<li><a href="/programs/graduate/mbahsm/">Health Services Management</a></li>
												<li><a href="/programs/graduate/mbatm/">Technology Management</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Accelerated<br>
											Programs (BS/MS)</h5>
											<ul class="list-unstyled">
												<li><a href="/undergraduate/cid/bsms_cididt">CID/IDT</a></li>
												<li><a href="/undergraduate/csc/accelerated_bsms">Computer and Information Science</a></li>
												<li><a href="/undergraduate/nur/degree_requirements">Nursing</a></li>
											</ul>
											<h5><strong>Certificate for<br>
											Advanced Study<br>
											Programs</strong></h5>
											<ul class="list-unstyled">
												<li><a href="/nursing.certs_anp_fnp">Adult Nurse Practitioner</a></li>
												<li><a href="/programs/graduate/data">Data Analysis</a> (post bachelor's)</li>
												<li><a href="/nursing.certs_anp_fnp">Family Nurse Practitioner</a> (post master's)</li>
												<li><a href="/nursing.certs_gnp">Gerontological Nurse Practitioner</a></li>
												<li><a href="/nursing.certs_nurse_admin">Nursing Administration</a></li>
												<li><a href="/nursing.certs_nursinged">Nursing Education</a> (post master's)</li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown yamm-fw menu-library">
						<a class="dropdown-toggle" data-hover="dropdown" data-delay="300" href="/library">Library</a>
						<ul class="dropdown-menu hidden-sm hidden-xs">
							<li>
								<div class="yamm-content">
									<h2>Library</h2>
									<div class="row">
										<div class="col-lg-2 col-md-2">
											<h5><a href="/library/">library.sunyit.edu</a></h5>
											<ul class="list-unstyled highlight">
												<li><a href="/library/ask_a_librarian">Ask a Librarian</a></li>
												<li><a href="http://ssl.sunyit.edu/apps/directory/?group=library&amp;format=short">Contact Us</a></li>
												<li><a href="/directions/">Directions to Campus</a></li>
												<li><a href="/library/hours">Hours</a></li>
												<li><a href="/library/lab">Lab Information</a></li>
												<li><a href="/library/off_campus">Off-Campus Users</a></li>
												<li><a href="/apps/calendar/main.php?calendar=libraryroom&amp;view=week">Room Event Schedule</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Research</h5>
											<ul class="list-unstyled">
												<li><a href="/library/catalogs">Catalogs</a></li>
												<li><a href="http://culturedmed.binghamton.edu/index.php/siteindex" target="_blank">CulturedMed</a></li>
												<li><a href="/library/faculty">Faculty Portal</a></li>
												<li><a href="/library/journal_lists">Journals</a></li>
												<li><a href="/library/guides">Research Guides</a></li>
												<li><a href="/library/tutorials/">Tutorials</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Services</h5>
											<ul class="list-unstyled">
												<li><a href="/library/databases">AskUs 24/7</a></li>
												<li><a href="/library/document_delivery">Document Delivery</a></li>
												<li><a href="/library/ereserves">E-Reserves</a></li>
												<li><a href="http://sunyit.illiad.oclc.org/illiad/logon.html" target="_blank">Interlibrary Loan</a></li>
												<li><a href="/library/library_instruction">Library Instruction</a></li>
												<li><a href="/library/reserves">Reserves</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Databases</h5>
											<ul class="list-unstyled">
												<li><a href="/library/databases/#business_db">Business/Management/<br>
												Accounting</a></li>
												<li><a href="/library/databases/#general_db">General Interest</a></li>
												<li><a href="/library/databases/#humanities_db">Humanities &amp; Education</a></li>
												<li><a href="/library/databases/#medicine_db">Medicine/Nursing/<br>
												Health</a></li>
												<li><a href="/library/databases/#newspapers_db">Newspapers</a></li>
												<li><a href="/library/databases/#science_db">Science &amp; Engineering</a></li>
												<li><a href="/library/databases/#social_db">Social Sciences</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Help Us Grow</h5>
											<ul class="list-unstyled">
												<li><a href="/apps/form/?resource=lib_suggest_a_book">Book Suggestions</a></li>
												<li><a href="/library/donations">Donations</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Copyright Information<br>
											&amp; Policies</h5>
											<ul class="list-unstyled">
												<li><a href="/library/copyright">Copyright Guidelines</a></li>
												<li><a href="/library/dmca/">Digital Millennium<br>
												Copyright Act (DMCA)</a></li>
												<li><a href="/library/policies">Policies &amp; Procedures</a></li>
												<li><a href="/library/circulation">Circulation</a></li>
												<li><a href="/library/ereserves">E-Reserves</a></li>
												<li><a href="/library/reserves">Reserves</a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown yamm-fw menu-wildcat-athletics">
						<a class="dropdown-toggle" data-hover="dropdown" data-delay="300" href="http://wildcats.sunyit.edu/">Wildcat Athletics</a>
						<ul class="dropdown-menu hidden-sm hidden-xs">
							<li>
								<div class="yamm-content">
									<h2>Wildcat Athletics</h2>
									<div class="row">
										<div class="col-lg-2 col-md-2">
											<h5><a href="http://www.wildcats.sunyit.edu">wildcats.sunyit.edu</a></h5>
											<ul class="list-unstyled highlight">
												<li><a href="http://wildcats.sunyit.edu/information/directory">Coaches/Staff</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/directions/">Directions to SUNYIT</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/facilities/">Facilities</a></li>
												<li><a href="/giving/">Give to SUNYIT</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/kids/">Kids' Night Out</a></li>
												<li><a href="http://wildcats.sunyit.edu/">Upcoming Events</a></li>
												<li><a href="http://wildcats.sunyit.edu/">Wildcat Headlines</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Men's Teams</h5>
											<ul class="list-unstyled">
												<li><a href="http://wildcats.sunyit.edu/sports/mbkb/">Basketball</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/bsb/">Baseball</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/mxc/">Cross Country</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/mlax/">Lacrosse</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/msoc/">Soccer</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/mvball/">Volleyball</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Women's Teams</h5>
											<ul class="list-unstyled">
												<li><a href="http://wildcats.sunyit.edu/sports/wbkb/">Basketball</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/wxc/">Cross Country</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/wlax/">Lacrosse</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/wsoc/">Soccer</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/sball/">Softball</a></li>
												<li><a href="http://wildcats.sunyit.edu/sports/wvball/">Volleyball</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>General Information</h5>
											<ul class="list-unstyled">
												<li><a href="http://wildcats.sunyit.edu/information/facilities/">Facilities</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/intramurals/">Intramurals</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/hall_of_fame/">Hall of Fame</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/overview/">Overview</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/sports_clinics/">Summer Clinics</a></li>
												<li><a href="/titleix/">Title IX</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/wildcat_open/">Wildcat Open</a></li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Student-Athletes</h5>
											<ul class="list-unstyled">
												<li><a href="http://wildcats.sunyit.edu/information/athletic_training/">Athletic Training</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/athletic_training/services">Athletic Training Services</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/athletic_training/forms/">Medical Forms</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/athletic_training/info/">Insurance Information/Forms</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/athletic_training/visiting/">Visiting Team Information</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/athletic_training/SUNYITwildcats_emergency_plan.pdf">Emergency Plan/Procedures</a> (pdf)</li>
											</ul>
										</div>
										<div class="col-lg-2 col-md-2">
											<h5>Student-Athletes</h5>
											<ul class="list-unstyled">
												<li><a href="http://wildcats.sunyit.edu/information/life_skills/">NCAA Champs/<br>
												Life Skills</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/saac/">SAAC</a></li>
												<li><a href="http://wildcats.sunyit.edu/information/handbook/">Student-Athlete Handbook</a></li>
											</ul>
											<h5>Conferences</h5>
											<ul class="list-unstyled">
												<li><a href="http://www.ecac.org/">ECAC</a></li>
												<li><a href="http://www.ncaa.com/">NCAA</a></li>
												<li><a href="http://www.neacsports.com/landing/index">NEAC</a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="menu-give">
						<a href="/giving">Give</a>
					</li>
				</ul>
        
      </div>

    </nav>

  </div>

</header>
