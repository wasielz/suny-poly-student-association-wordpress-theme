<div class="page-header">
	<div class="breadcrumbs">
		<?php if (function_exists('bootstrap_breadcrumbs')): ?>
			<?php bootstrap_breadcrumbs(); ?>
		<?php endif; ?>
	</div>
  <h1>
    <?php echo roots_title(); ?>
  </h1>
</div>
